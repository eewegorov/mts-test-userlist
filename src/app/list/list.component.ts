import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { SubscriptionLike } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { UserDataExtended } from '../models';
import { DataService } from '../services/data.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements AfterViewInit, OnDestroy {
  public displayedColumns: string[] = ['avatar', 'name', 'username', 'email', 'address'];
  public dataSource: MatTableDataSource<UserDataExtended>;
  public companies: string[] = ['Сбросить фильтр'];
  public sortings: string[] =
    ['Name asc', 'Name desc', 'Username asc', 'Username desc', 'Email asc', 'Email desc', 'Address asc', 'Address desc'];
  public sorting = 'Name asc';
  private usersSub: SubscriptionLike;

  @ViewChild(MatPaginator) private paginator: MatPaginator;
  @ViewChild(MatSort) private sort: MatSort;

  constructor(private dataService: DataService) { }

  ngAfterViewInit(): void {
    this.usersSub = this.dataService.getUsers().subscribe((users: UserDataExtended[]) => {
      this.dataSource = new MatTableDataSource(users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.filterPredicate = (data: UserDataExtended, filter: string) => {
        return filter !== 'сбросить фильтр' ? data.company.name.trim().toLowerCase().indexOf(filter) !== -1 : true;
      };
      users.forEach((user: UserDataExtended) => {
        this.companies.push(user.company.name);
      });
    });
  }

  public applyFilter(company: string): void {
    this.dataSource.filter = company.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public applySort(sort: string): void {
    const id = sort.split(' ')[0].toLowerCase();
    const start = sort.split(' ')[1] === 'asc' ? 'asc' : 'desc';
    if (this.sort.active === id && this.sort.direction === start) {
      return;
    }
    this.sort.sort({ id, start, disableClear: true });
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    if (this.usersSub) {
      this.usersSub.unsubscribe();
    }
  }

}
