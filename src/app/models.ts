export interface UserData {
  id: number;
  name: string;
  username: string;
  email: string;
  phone: string;
  website: string;
  company: Company;
}

export interface UserDataResponse extends UserData {
  address: Address;
}

export interface UserDataExtended extends UserData {
  address: string;
  avatar: string;
}

export interface Company {
  name: string;
  catchPhrase: string;
  bs: string;
}

export interface Address {
  street: string;
  suite: string;
  city: string;
  zipcode: string;
  geo: Geo;
}

export interface Geo {
  lat: string;
  lng: string;
}

export interface UserTodo {
  userId: number;
  id: number;
  title: string;
  completed: boolean;
}
