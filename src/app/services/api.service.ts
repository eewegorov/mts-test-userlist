import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { UserDataResponse, UserTodo } from '../models';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  public getUsers(): Observable<UserDataResponse[]> {
    return this.http.get<UserDataResponse[]>(`${environment.apiUrl}/users`);
  }

  public getUser(id: number): Observable<UserDataResponse[]> {
    return this.http.get<UserDataResponse[]>(`${environment.apiUrl}/users?id=${id}`);
  }

  public getUserTodos(userId: number): Observable<UserTodo[]> {
    return this.http.get<UserTodo[]>(`${environment.apiUrl}/todos?userId=${userId}`);
  }

}
