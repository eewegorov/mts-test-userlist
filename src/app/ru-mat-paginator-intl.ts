import { MatPaginatorIntl } from '@angular/material/paginator';
import { Injectable } from '@angular/core';

@Injectable()
export class RuMatPaginatorIntl extends MatPaginatorIntl {
    nextPageLabel = 'След. страница';
    previousPageLabel = 'Пред. страница';
    itemsPerPageLabel = 'Кол-во записей на странице:';
    firstPageLabel = 'Первая страница';
    lastPageLabel = 'Последняя страница';
}
