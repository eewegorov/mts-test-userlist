import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SubscriptionLike } from 'rxjs';
import { DataService } from '../services/data.service';
import { UserDataExtended, UserTodo } from '../models';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {
  public user = {} as UserDataExtended;
  public dataSource: MatTableDataSource<UserTodo>;
  public displayedColumns: string[] = ['id', 'title', 'completed'];
  private userSub: SubscriptionLike;
  private todosSub: SubscriptionLike;

  @ViewChild(MatSort) private sort: MatSort;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService
  ) { }

  ngOnInit(): void {
    const userId = this.route.snapshot.params.id;
    this.userSub = this.dataService.getUser(userId).subscribe((user: UserDataExtended) => {
      if (user === null) {
        this.router.navigate(['user']);
      }

      this.user = user;
    });

    this.todosSub = this.dataService.getUserTodos(userId).subscribe((todos: UserTodo[]) => {
      this.dataSource = new MatTableDataSource(todos);
      this.dataSource.sort = this.sort;
    });
  }

  ngOnDestroy(): void {
    if (this.userSub) {
      this.userSub.unsubscribe();
    }

    if (this.todosSub) {
      this.todosSub.unsubscribe();
    }
  }



}
